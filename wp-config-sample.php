<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|U:-dR45J*B#^ghbC(v#5$b`;w!(n$bIQ-QN:F!Ke^WS[wg:1.KH oPJwu$Qaq0[');
define('SECURE_AUTH_KEY',  '/<C!x=Pn>CGSM+9xC8z<U{&Q.Q.4mJ4,f3? 9oGQdH*+3.fV3$^xs$JzEc3<5zho');
define('LOGGED_IN_KEY',    'A-1s_a07-pxays{wRlrq47T-?K8iC(BOk[_AL6=*`{`!6]NpahC`uZGl6dZ2le1)');
define('NONCE_KEY',        'zNzFQzJ`noGaEs!tTr.>$H[|brQAX2*wDH0CRm11bgKsnG1a^GiQEs#&c/+*lXS+');
define('AUTH_SALT',        'E]^#i`8BNn6Jbh{2L/L*I`-X@OO`OGd!;NLwK`Jg)Zu/~ll!<}D|MN=R@gT;zqp7');
define('SECURE_AUTH_SALT', 'zxEG97) *]q_U|pze1MxA?_m_X(30PFzo9yPfLgggP5cFvuSo+8e_;`]O?2dAQ(B');
define('LOGGED_IN_SALT',   '?BsEJ<g/ _0*:VFb5}|;b):j7&974eK%.BY&<zJ>,S!Yp%;WiZ.D8j4Q%:JLnus}');
define('NONCE_SALT',       'aNDX^O$!Dp(bTu|bXJ0>Q$WFlNy. ]yX c<K!dcG#h_0VQb2wh{*:kraqh6cm$!K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


