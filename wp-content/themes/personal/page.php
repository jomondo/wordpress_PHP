<?php  
	/* template to show dinamic pages*/
	get_header();

?>
<div class="row">
	<div class="col-md-12">
		<?php
			//print content loop
			while ( have_posts() ) : the_post(); 

		?>
			<article <?php post_class(); ?> >
				<div class="entry-header">
					<!-- print title of db -->
					<!--<h1 class="entry-title"><?php //the_title(); ?></h1>-->
					
				</div>
				<!-- print content of db -->
				<div class="entry-content-page">
					<?php the_content(); ?>
				</div>

			</article>

		<?php 
			endwhile;

		?>

	</div>
</div>

<?php 
	get_footer();
	
?>
