<?php
	function sendtoken($arrArgument, $type) {
	    $mail = array(
	        'type' => $type,
	        'token' => $arrArgument['token'],
	        'inputEmail' => $arrArgument['email']
	    );
	    
	    try {
	        send_email($mail);
	        return true;

	    } catch (Exception $e) {
	        return false;
	        
	    }
	    
	}

	function amigable($ruta) {
	    return "http://localhost/wordpress/".$ruta;
	    
	}
