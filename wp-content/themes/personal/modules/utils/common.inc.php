<?php
    function loadModel($model_path, $model_name, $function, $arrArgument = '') {
        $model = $model_path . $model_name . '.class.singleton.php';
        /*echo $model;
        exit;*/
        if (file_exists($model)) {
            include_once($model);

            $modelClass = $model_name;
            if (!method_exists($modelClass, $function)){
                die($function . ' function not found in Model ' . $model_name);
            }
            
            $obj = $modelClass::getInstance();
            if (isset($arrArgument)) {
                return $obj->$function($arrArgument);
            }
        } else {
            die($model_name . ' Model Not Found under Model Folder');
        }
    }

    /*function loadView($name_module,$view){
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menuUser.php");
        
        $path = MODULE_PATH .$name_module."/view/".$view.".html";
        if (file_exists($path)) {
            include($path);

        }else{
            showErrorPage(0, "ERROR - 404 not found view", 'HTTP/1.0 404 Bad Request', 404);

        }

        require_once(VIEW_PATH_INC . "footer.html");

    }*/
