<?php get_header();	?>
<!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>-->
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/modules/login/view/css/style-login-register.css">
<script src="<?php blogInfo('template_url'); ?>/modules/login/view/js/register.js"></script>

<section class="login-block">
	<!-- errors form -->
	<span id="errors"></span>
	<!-- errors form -->
    <div class="container container-login">
	<div class="row">
		<div class="col-md-4 login-sec">
		    <h2 class="text-center">Regístrate</h2>
		    <form class="form-area contact-form text-left" id="myForm">
			  <div class="form-group">
			    <label for="input-user" class="text-uppercase">Usuario</label>
			    <input type="text" class="form-control" placeholder="" id="user-register" name="user-register">
			    
			  </div>
			  <div class="form-group">
			    <label for="input-email" class="text-uppercase">Email</label>
			    <input type="email" class="form-control" placeholder="" id="email-register" name="email-register">
			  </div>

			  <div class="form-group">
			    <label for="input-passwd" class="text-uppercase">Contraseña</label>
			    <input type="password" class="form-control" placeholder="" id="passwd-register" name="passwd-register">
			  </div>

			  <div class="form-group" style="float: left;">
			  	<button class="btn btn-login float-right" id="btn-register" >Registrarme</button>
			  </div>
			</form>

			<div class="copy-text"><a href="http://localhost/wordpress/index.php/iniciar-session/">¿Ya tienes una cuenta?</a></div>
		</div>
		<div class="col-md-8 banner-sec">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
            <div class="carousel-inner" role="listbox">
			    <div class="carousel-item active">
			      <img class="d-block img-fluid" src="<?php blogInfo('template_url');?>/assets/img/slider/programmer.jpg" alt="First slide">
			      <div class="carousel-caption d-none d-md-block">
			        <div class="banner-text">
			            <h2>This is Heaven</h2>
			            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
			        </div>	
			  </div>
			    </div>
			    <div class="carousel-item">
			      <img class="d-block img-fluid" src="<?php blogInfo('template_url');?>/assets/img/slider/coding.jpg" alt="First slide">
			      <div class="carousel-caption d-none d-md-block">
			        <div class="banner-text">
			            <h2>This is Heaven</h2>
			            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
			        </div>	
			    </div>
			    </div>
			    <div class="carousel-item">
			      <img class="d-block img-fluid" src="<?php blogInfo('template_url');?>/assets/img/slider/programmer-women.jpg" alt="First slide">
			      <div class="carousel-caption d-none d-md-block">
			        <div class="banner-text">
			            <h2>This is Heaven</h2>
			            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
			        </div>	
			    </div>
			  </div>
            </div>	   
		    
		</div>
	</div>
</div>
</section>

<?php get_footer(); ?>
