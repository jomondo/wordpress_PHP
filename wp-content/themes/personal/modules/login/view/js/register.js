$('document').ready(function() {
	$(document).on('click','#btn-register',function(){
		var rdo = true;
        
        var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var emailreg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
        var passwdreg = /^[A-Za-z0-9ñÑ]{5,20}$/;
            
        var user_name = $("#user-register").val();
        var email = $("#email-register").val();
        var passwd = $("#passwd-register").val();
        
        if( user_name == ""){
            var message = "Introduce su usuario";
            alerts("errors",message, "danger");
            rdo=false;
            return false;
        }else if (!namereg.test(user_name)){
          var message = "El usuario tiene que tener entre 4-35 caracteres";
          alerts("errors",message, "danger");
          rdo=false;
          return false;
        }if( email == ""){
            var message = "Introduce su usuario";
            alerts("errors",message, "danger");
            rdo=false;
            return false;
        }else if (!emailreg.test(email)){
          var message = "Tiene que ser un email válido";
          alerts("errors",message,"danger");
          rdo=false;
          return false;
        }if( passwd == ""){
            var message = "Introduce su contraseña";
            alerts("errors",message,"danger");
            rdo=false;
            return false;
        }else if (!passwdreg.test(passwd)){
            var message = "La contraseña tiene que ser alfanumerica entre 5-20 caracteres";
            alerts("errors",message,"danger");
            rdo=false;
            return false;
        }

        if (rdo) {
          var data = { "id":user_name, "email":email, "password":passwd };

          $.post(amigable("?module=login&function=register"),{datas: data},function (response){
            console.log(response);
            var json = JSON.parse(response);
            if (json.error.id) {
              var message = json.error.id;
              alerts("errors",message,"danger");

            }
                
          }).fail(function(xhr){
            console.log(xhr.responseText);    

          });

        }//end rdo

	});

});
