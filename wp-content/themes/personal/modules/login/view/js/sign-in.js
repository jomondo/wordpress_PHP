$('document').ready(function() {
	$(document).on('click','#btn-login',function(){
		var rdo = true;
        
        var namereg = /^[A-Za-z\sñÑ]{4,35}$/;
        var passwdreg = /^[A-Za-z0-9ñÑ]{5,20}$/;
            
        var user_name = $("#user-login").val();
        var passwd = $("#passwd-login").val();
        
        if( user_name == ""){
            var message = "Introduce su usuario";
            alerts("errors",message, "danger");
            rdo=false;
            return false;
        }else if (!namereg.test(user_name)){
          var message = "El usuario tiene que tener entre 4-35 caracteres";
          alerts("errors",message, "danger");
          rdo=false;
          return false;
        }if( passwd == ""){
            var message = "Introduce su contraseña";
            alerts("errors",message,"danger");
            rdo=false;
            return false;
        }else if (!passwdreg.test(passwd)){
            var message = "La contraseña tiene que ser alfanumerica entre 5-20 caracteres";
            alerts("errors",message,"danger");
            rdo=false;
            return false;
        }

        if (rdo) {
          var data = { "id":user_name, "password":passwd };

          $.post(amigable("?module=login&function=sign_in"),{datas: data},function (response){
            console.log(response);
            var json = JSON.parse(response);
            //console.log(json.error.id);
            if (json.error!=undefined) {
              if (json.error.id) {
                var message = json.error.id;
                alerts("errors",message,"danger");

              }

              if (json.error.password) {
                var message = json.error.password;
                alerts("errors",message,"danger");

              }

            }else if(json.success){
              console.log(json.user);
            }
            
                
          }).fail(function(xhr){
            console.log(xhr.responseText);    

          });

        }//end rdo

	});

});
