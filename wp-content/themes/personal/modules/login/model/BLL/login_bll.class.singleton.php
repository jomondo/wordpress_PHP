<?php    
    include("wp-content/themes/personal/modules/login/model/DAO/login_dao.class.singleton.php");
    
    class login_bll {
        private $dao;
        private $db;
        static $_instance;

        private function __construct() {
            $this->dao = login_dao::getInstance();

        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function count_user_login_BLL($array) {
            return $this->dao->count_user_login_DAO($array);
        }

        public function create_user_BLL($array) {
            return $this->dao->create_user_DAO($array);
        }

        public function update_activate_BLL($array) {
            return $this->dao->update_activate_DAO($array);
        }

        public function count_login_BLL($array) {
            return $this->dao->count_login_DAO($array);
        }

        public function select_user_BLL($array) {
            return $this->dao->select_user_DAO($array);
        }

        public function count_activate_BLL($array) {
            return $this->dao->count_activate_DAO($array);
        }

    }