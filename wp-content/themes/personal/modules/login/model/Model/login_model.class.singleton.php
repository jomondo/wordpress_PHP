<?php
    include("wp-content/themes/personal/modules/login/model/BLL/login_bll.class.singleton.php");

    class login_model {
        private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = login_bll::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function count_user_login($array) {
            return $this->bll->count_user_login_BLL($array);

        }

        public function create_user($array) {
            return $this->bll->create_user_BLL($array);
            
        }

        public function update_activate($array) {
            return $this->bll->update_activate_BLL($array);
            
        }

        public function count_login($array) {
            return $this->bll->count_login_BLL($array);

        }

        public function select_user($array) {
            return $this->bll->select_user_BLL($array);

        }

        public function count_activate($array) {
            return $this->bll->count_activate_BLL($array);

        }
        
    }