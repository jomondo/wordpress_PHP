<?php
    class login_dao {
        static $_instance;

        private function __construct() {
            
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function count_user_login_DAO($array) {
            global $wpdb;
            $table = $wpdb->base_prefix."user_register";

            $registros = $wpdb->get_results( "SELECT count(*) as count FROM $table WHERE id_user = '".$array['id']."' or email='".$array['email']."' " );
            return $registros;

        }

        public function create_user_DAO($array) {
            global $wpdb;

            return $wpdb->insert('wp_user_register', array(
                'id_user' => $array['id'],
                'name' => $array['name'],
                'passwd' => $array['passwd'],
                'email' => $array['email'],
                'avatar' => '/media/test2.jpg',
                'type' => 0,
                'token' => $array['token'],
                'date' => '26/07/1991',
                'activate' => $array['activate'],
            ));

        }

        public function update_activate_DAO($array) {
            global $wpdb;

            return $wpdb->update(
                'wp_user_register', 
                array(
                    'activate' => 1,
                ), 
                array( 'token'=> $array['token'] ));

        }

        public function count_login_DAO($array) {
            global $wpdb;
            $table = $wpdb->base_prefix."user_register";

            $registros = $wpdb->get_results( "SELECT count(*) as count FROM $table WHERE id_user = '".$array['id']."' " );
            return $registros;

        }

        public function select_user_DAO($array) {
            global $wpdb;
            $table = $wpdb->base_prefix."user_register";

            $registros = $wpdb->get_results( "SELECT * FROM $table WHERE id_user = '".$array['id']."' " );
            return $registros;

        }

        public function count_activate_DAO($array) {
            global $wpdb;
            $table = $wpdb->base_prefix."user_register";

            $registros = $wpdb->get_results( "SELECT count(*) as count FROM $table WHERE id_user = '".$array['id']."' and activate = 1 " );
            return $registros;

        }

    }