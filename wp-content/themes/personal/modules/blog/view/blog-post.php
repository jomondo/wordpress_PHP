<!-- template for displaying author, categories, post-types, taxonomies, date and tag -->

			<!-- start banner Area -->
			<?php  
				breadcrumb();

			?>
			<!-- End banner Area -->
			
			<!-- Start post-content Area -->
			<section class="post-content-area" style="margin-top: 40px;">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 posts-list">
							<?php 
								while ( have_posts() ) : the_post();

							?>
							<div class="single-post row">
								<!--<div class="col-lg-3  col-md-3 meta-details">	
									
								</div>-->
								<div class="article-post" style="background-color: #e6e0de;border-radius: 8px;" >
									<div class="box-article" style="background-color: #8490ff;border-radius: 5px;">
										<?php
											if (has_post_thumbnail()) {
												the_post_thumbnail();
													
											}
										?>
										
										<a href="<?php echo get_permalink(); ?>" class="permalink"><?php _e(the_title(), 'personal'); ?></a>

									</div>
	
									<!--<a href="<?php echo get_permalink(); ?>" class="permalink"><?php _e(the_title(), 'personal'); ?></a>--><!-- devuelve enlace de la entrada-->

									<ul class="tags" style="margin-top: 10px;">
										<!-- get tags post -->
										<?php 
											global $post;	
										    $tags = wp_get_post_tags($post->ID);

										    foreach ($tags as $tag) {		
										        $tag_link = get_tag_link($tag->term_id);

									    ?>
									        	<a href="<?php echo $tag_link; ?>" class="tags_article" style=""><?php echo $tag->name; ?></a>
									    <?php
									    	}
									    	
										?>

									</ul>
									<div style="padding: 10px;"><p class="excert" > <?php the_excerpt(); ?> </p></div>
									
									<div class="details">
										<a href="<?php echo get_permalink(); ?>" class="btn-read-article"><?php _e('Seguir leyendo', 'personal'); ?></a>
										<a href="#" style="margin-left: 20px;"><?php the_author(); ?></a> <span class="lnr lnr-user"></span>
										<a href="#" style="margin-left: 20px;"><?php echo get_comments_number(); ?> Comments</a> <span class="lnr lnr-bubble"></span>
										<a href="#" style="margin-left: 20px;"><?php echo get_the_date(); ?></a> <span class="lnr lnr-calendar-full"></span>
									</div>
								</div>
							</div>
							
							<?php 
								endwhile;
								//pagination 
								custom_pagination();

							?>

						</div>
						<div class="col-lg-4 sidebar-widgets">
							<div class="widget-wrap">
								<div class="single-sidebar-widget search-widget">
									<form class="search-form" action="#">
			                            <input placeholder="Search Posts" name="search" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'" >
			                            <button type="submit"><i class="fa fa-search"></i></button>
			                        </form>
								</div>
								<div class="single-sidebar-widget user-info-widget">
									<img src="img/blog/user-info.png" alt="">
									<a href="#"><h4>Joan Montés</h4></a>
									<p>Senior blog writer</p>
									<ul class="social-links">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-github"></i></a></li>
										<li><a href="#"><i class="fa fa-behance"></i></a></li>
									</ul>
									<p>
										Boot camps have its supporters andit sdetractors. Some people do not understand why you should have to spend money on boot camp when you can get. Boot camps have itssuppor ters andits detractors.
									</p>
								</div>
								<div class="single-sidebar-widget popular-post-widget">
									<h4 class="popular-title"><?php _e('Ultimos Posts','personal'); ?></h4>
									<div class="popular-post-list">
										<div class="single-post-list d-flex flex-row align-items-center" style="display: block !important;float: left !important;">
											<!--<div class="thumb">
												<img class="img-fluid" src="img/blog/pp1.jpg" alt="">
											</div>
											<div class="details">
												<a href="blog-single.html"><h6>Space The Final Frontier</h6></a>
												<p>02 Hours ago</p>
											</div>-->
											<?php query_posts('showposts=5'); /*limitamos el resultado a 5 resultados */ ?>
											<?php if (have_posts()) : while (have_posts()) : the_post();?>
											     <!--<ul>
											         <li>
											            <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
											         </li>
											     </ul>-->
											     <div class="details">
													<a href="<?php the_permalink();?>" ><h6><?php the_title();?></h6></a>
													<!--<p>02 Hours ago</p>-->
												</div>
											<?php endwhile; endif;?>
										</div>
																								
									</div>
								</div>
								<div class="single-sidebar-widget ads-widget">
									<a href="#"><img class="img-fluid" src="img/blog/ads-banner.jpg" alt=""></a>
								</div>
								<div class="single-sidebar-widget post-category-widget">
									<h4 class="category-title"><?php _e('Categorias','personal'); ?></h4>
									<ul class="cat-list">
										<?php wp_list_cats('use_desc_for_title=1'); ?>													
									</ul>
									<!--$category = get_the_category();
									echo $category[0]->category_count;-->
									
									<ul class="cat-list">
							<?php 
								/*if (is_active_sidebar('sidebar-posts')) : 
									dynamic_sidebar('sidebar-posts');
								endif;*/
							?>
																					
									</ul>
								</div>	
								<div class="single-sidebar-widget newsletter-widget">
									<h4 class="newsletter-title">Newsletter</h4>
									<p>
										Suscríbete a mi newsletter para estar al día sobre mis posts
									</p>
									<div class="form-group d-flex flex-row">
									   <div class="col-autos">
									      <div class="input-group">
									        <div class="input-group-prepend">
									          <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i>
											</div>
									        </div>
									        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" >
									      </div>
									    </div>
									    <a href="#" class="bbtns">Subcribe</a>
									</div>	
									<p class="text-bottom">
										Tambíen puedes desuscribirte si lo deseas. No me gusta el spam.
									</p>								
								</div>
								<div class="single-sidebar-widget tag-cloud-widget">
									<h4 class="tagcloud-title">Tag Clouds</h4>
									<ul>
										<!-- get all tags -->
										<?php wp_tag_cloud(); ?>
									</ul>

								</div>								
							</div>
						</div>
					</div>
				</div>	
			</section>
			<!-- End post-content Area -->