$(document).on('click','#send_msg',function(){
	var name = $("#name").val();
	var email = $("#email").val();
	var subject = $("#subject").val();
	var message = $("#message").val();

	if (name !=="" && email !=="" && subject !=="" && message !=="" ) {
		var data = { "name":name, "email":email, "subject":subject, "message":message};
        var data_val_JSON = JSON.stringify(data);

        $.post('http://localhost/wordpress/wp-content/themes/personal/modules/contact/controller/controller_mail.php',{data: data_val_JSON},function (response){
        	if (response == "") {
				alert("Verifique en su correo que se ha entregado correctamente.");
				$("#name").val('');
				$("#email").val('');
				$("#subject").val('');
				$("#message").val('');

        	}
            
        }).fail(function(xhr){
            alert(xhr.responseText);    

        });

	}

});

/* init static map */
function initMap() {
	var uluru = {lat: 38.820785, lng: -0.610514};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 13,
		center: uluru
	});

	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});

}