<?php 
include("../utils/email.inc.php");

if (isset($_POST["data"])) {
	$dataJSON = json_decode($_POST["data"], true);
	

	$arrValues = array(
		'type' => "contact",
		'name' => $dataJSON['name'],
		'email' => $dataJSON['email'],
		'subject' => $dataJSON['subject'],
		'message' => $dataJSON['message']
	);

	try{
        send_email($arrValues);

	}catch (Exception $e) {
		//showErrorPage(0, "ERROR - 404", 'HTTP/1.0 404 Bad Request', 404);
		echo "Error 404";

	}

	/* envio email admin */
	$arrValues = array(
		'type' => "admin",
		'name' => $dataJSON['name'],
		'email' => $dataJSON['email'],
		'subject' => $dataJSON['subject'],
		'message' => $dataJSON['message']
	);

	try{
		sleep(5);
        send_email($arrValues);

	}catch (Exception $e) {
		//showErrorPage(0, "ERROR - 404", 'HTTP/1.0 404 Bad Request', 404);
		echo "Error 404";

	}

}else{
	echo "Error 404";
	
}