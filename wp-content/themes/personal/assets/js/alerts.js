//console.log("estic en alerts");
function alerts(element, message, type){
	$(".alert-danger").remove();//delete alert to not repeat it
	if (type=="danger") {
		$("#"+element).append('<div class="alert alert-danger" role="alert">'+message+'</div>').fadeIn(2000);
		$("#"+element).fadeOut(15000);

	}else if(type == "warning"){
		$("#"+element).append('<div class="alert alert-warning" role="alert">'+message+'</div>').fadeIn(2000);
		$("#"+element).fadeOut(15000);

	}else if(type == "success"){
		$("#"+element).append('<div class="alert alert-success" role="alert">'+message+'</div>').fadeIn(2000);
		$("#"+element).fadeOut(15000);

	}else if(type == "info"){
		$("#"+element).append('<div class="alert alert-info" role="alert">'+message+'</div>').fadeIn(2000);
		$("#"+element).fadeOut(15000);

	}else{
		console.log("Querido developer, te has equivocado en el tipo de alert");

	}
	

}