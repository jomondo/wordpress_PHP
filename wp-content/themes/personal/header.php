<!-- template download here -> https://colorlib.com/wp/template/personal/ -->
<!-- 
	designed by Colorlib
	developed by Joan Montés 
-->

<!DOCTYPE html>
	<html <?php language_attributes(); ?> class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?php blogInfo('template_url'); ?>/assets/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Personal</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!-- CSS -->
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/linearicons.css">
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/bootstrap.css">
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/magnific-popup.css">
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/jquery-ui.css">				
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/nice-select.css">							
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/animate.min.css">
			<link rel="stylesheet" href="<?php blogInfo('template_url'); ?>/assets/css/owl.carousel.css">				
			<link rel="stylesheet" href="<?php blogInfo('stylesheet_url'); ?>">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		
			<?php wp_head(); ?><!-- añadir de forma automatica css plugins,seo,..-->
		</head>
		<body <?php body_class(); ?> ><!-- añade classes donde nos encotramos-->	
		  <header id="header">
		    <div class="container main-menu">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href="index.html"><img src="<?php blogInfo('template_url'); ?>/assets/img/logo.png" alt="" title="" /></a>
			      </div>
			      <?php
			      	/* nav menu */
			      	wp_nav_menu( array( 
			      		'theme_location' => 'top', 
			      		'container' => 'nav',
			      		'container_class' => 'nav-menu-container',
			      		'menu_class' => 'nav-menu',

			      	));
			      ?>
			      	    		
		    	</div>
		    </div>
		  </header><!-- #header -->