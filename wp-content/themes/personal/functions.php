<?php

/**
* personal functions and definitions
*/

/* enable suport for post tumbnails on posts and pages*/
add_theme_support('post-thumbnails');

add_image_size('personal-featured-image', 2000, 1200, true);

add_image_size('personal-thumbnail-avatar', 100, 100, true);

/* this theme uses wp_nav_menu() in thow locations */
register_nav_menus( array(
'top' => __('Top Menu', 'personal'),
'footer' => __('Footer Menu', 'personal'),

));

/* example add classes to main menu */

/*function personal_test_classes ($classes, $item, $args){
	if ($args->theme_location == 'top') {
		$classes[] = 'name_item';

	}
	return $classes;

}
/* 1 parametrer -> property wordpress 
2 parametrer -> execut our function
*/

/*add_filter('nav_menu_css_class', 'personal_test_classes',1,3);*/

function catch_that_image() {
  	global $post, $posts;
  	$first_img = '';
  	ob_start();
  	ob_end_clean();
  	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  	$first_img = $matches [1] [0];
  	$first_img ='<img class="img-fluid" src="'.$first_img.'" style="with:750px;height:350px;" alt=""/>';
 
  	if(empty($first_img)){ //Defines a default image
    	$first_img = " ";
  	}

  	return $first_img;

}
/* Register sidebars */
add_action('widgets_init','personal_widgets_init');

function personal_widgets_init(){
	register_sidebar( 
		array( 'name' => __('Posts Sidebar', 'theme-slug'),
				'id' => 'sidebar-posts',
				'description' => __('widgets in this area will be shown on all posts and pages','theme-slug'),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</li>',
				'before_title' => '<h2 class="widgettitle">',
				'after_title' => '</h2>',
		));
}

// Comments

function move_comment_field( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'move_comment_field' );

function personal_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
			?>
		    <li class="comment">
		        <p><?php _e( 'Pingback:', 'personal' ); ?> <?php comment_author_link(); ?>
		        	<?php edit_comment_link( __( '(Editar)', 'personal' ), ' ' ); ?></p>
		    	<?php
		    break;

		default : ?>
			<li <?php comment_class('comment'); ?> id="li-comment-<?php comment_ID(); ?>">
				<article id="comment-<?php comment_ID(); ?>">
					<div class="comment-meta">
		                <div class="comment-author vcard">

		                	<?php $args = [
		                        "class" => "avatar avatar-60 photo"
		                    ];
		                    echo get_avatar( $comment, 60 ); ?>

		                    <?php
		                    printf( __( '<b class="fn">%s</b> <span class="says hide">dice:</span>', 'personal' ),
	                    	sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) );
	                    	?>

	                    	<div class="comment-content">
	                    		<?php comment_text(); ?>
	                    	</div>

	                    	<div class="comment-metadata">
                    			<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
		                            <time pubdate datetime="<?php comment_time( 'c' ); ?>">
		                                <?php
		                                /* translators: 1: date, 2: time */
		                                printf(
		                                	__( '%1$s at %2$s', 'personal' ),
		                                	get_comment_date(),
		                                	get_comment_time() );
		                                ?>
		                            </time>
		                        </a>
		                        <?php edit_comment_link( __( '(Editar)', 'personal' ), ' ' ); ?>
	                    	</div>

	                    	<div class="reply">
                    			<?php post_reply_link(); comment_reply_link( array_merge( $args, array( 'depth' => $depth ) ) ); ?>
	                    	</div>

	                    	<?php if ( $comment->comment_approved == '0' ) : ?>
	                    		<em><?php _e( 'Su comentario tiene que ser aprovado por el administrador.', 'personal' ); ?></em>
	                    	<?php endif; ?>

		                </div>
		            </div>
				</article>
			</li>
			<?php
			break;
	endswitch;
}

function pagination_anterior_siguiente() {
    global $wp_query;
 
    if ( $wp_query->max_num_pages > 1 ) { ?>
        <nav class="pagination" role="navigation">
            <div class="nav-previous"><?php next_posts_link( '&larr; Anterior',true ); ?></div>
            <div class="nav-next"><?php previous_posts_link( 'Siguiente &rarr;' ,true); ?></div>
        </nav>
<?php }
 wp_reset_postdata();
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {
	if (empty($pagerange)) {
		$pagerange = 2;
	}
	 
	/**
	* This first part of our function is a fallback
	* for custom pagination inside a regular loop that
	* uses the global $paged and global $wp_query variables.
	* 
	* It's good because we can now override default pagination
	* in our theme, and use this function in default quries
 	* and custom queries.
	*/

	global $paged;
	if (empty($paged)) {
		$paged = 1;
	}

	if ($numpages == '') {
		global $wp_query;
	    $numpages = $wp_query->max_num_pages;
	    if(!$numpages) {
	    	$numpages = 1;
	    }

	}
	 
	  /** 
	   * We construct the pagination arguments to enter into our paginate_links
	   * function. 
	   */


	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array( 'post_type' => 'eventos','posts_per_page' =>8, 'order' => 'DESC', 'paged' => $paged );
	$pagination_args = array(
	    'base'            => get_pagenum_link(1) . '%_%',
	    'format'          => 'page/%#%',
	    'total'           => $numpages,
	    'current'         => $paged,
	    'show_all'        => False,
	    'end_size'        => 1,
	    'mid_size'        => $pagerange,
	    'prev_next'       => True,
	    'prev_text'       => __('&laquo;'),
	    'next_text'       => __('&raquo;'),
	    'type'            => 'plain',
	    'add_args'        => false,
	    'add_fragment'    => ''
	);
	 
	$paginate_links = paginate_links($pagination_args);
	 
	if ($paginate_links) {
	    echo "<nav class='custom-pagination d-flex'>";
	    	//echo "<span class='page-numbers page-num'>Total Pages: ". $numpages . "</span> ";
	    	echo $paginate_links;
	    echo "</nav>";

	}
 
}

/* template contact */
function shortcode_template_contact (){
	include (TEMPLATEPATH . '/modules/contact/view/contact.php');
	
}

add_shortcode( 'shortcode', 'shortcode_template_contact' );

/* template register & login */
function shortcode_template_login (){
	include (TEMPLATEPATH . '/modules/login/view/login.html');
	
}

add_shortcode( 'shortcode-register-login', 'shortcode_template_login' );

function shortcode_template_register (){
	include (TEMPLATEPATH . '/modules/login/view/register.php');
	
}

add_shortcode( 'shortcode-register', 'shortcode_template_register' );

function shortcode_template_router (){
	include (TEMPLATEPATH . '/router.php');
	
}

add_shortcode( 'shortcode-router', 'shortcode_template_router' );

//*********** TEST to get info db *************//

/*
global $wpdb;
$table = $wpdb->base_prefix."comments";

	global $wpdb;
	$registros = $wpdb->get_results( "SELECT * FROM $table" );
	echo "Registro #1. Comment_id: " . $registros[0]->comment_ID . ", Author: " . $registros[0]->comment_author. "<br/>";*/



function breadcrumb(){
	// start banner Area
	?>
			<section class="relative about-banner">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								<?php 
								if (is_single()) {
									echo "Detalles artículo";

								}else{
									if (is_category()) {
										single_cat_title();

									}
								} 

								?>				
							</h1>
							<?php
								if (is_single()) {
							?>
									<p class="text-white link-nav"><a href="http://localhost/wordpress/">Home </a>  <span class="lnr lnr-arrow-right"></span><a href="../../../../category/blog">Blog </a> <span class="lnr lnr-arrow-right"></span> <a href="#"> Blog detalles artículo</a></p>
							<?php			
								}else{
									echo '<p class="text-white link-nav"><a href="http://localhost/wordpress/">Home </a>  <span class="lnr lnr-arrow-right"></span><a href="http://localhost/wordpress/index.php/category/blog/">Blog </a></p>';
								}
							?>
							
						</div>	
					</div>
				</div>
			</section>
			
<?php  
}
