<?php

include("wp-content/themes/personal/modules/utils/common.inc.php");
include("wp-content/themes/personal/modules/utils/functions.inc.php");
include("wp-content/themes/personal/modules/utils/email.inc.php");
ob_start(); 
//session_start();

function handlerRouter() {
    if (!empty($_GET['module'])) {
        $URI_module = $_GET['module'];

    } else {
    	$URI_module = "home";

    }

    if (!empty($_GET['function'])) {
        $URI_function = $_GET['function'];

    } else {
        $URI_function = 'begin';
        
    }
    handlerModule($URI_module, $URI_function);
}

function handlerActivate() {
    if (isset($_GET['activate'])) {
        $_GET['module'] = "home";
        $_GET['function'] = "begin";
        $_GET['act'] = true;

    }
}

function handlerModule($URI_module, $URI_function) {
    $modules = simplexml_load_file('wp-content/themes/personal/resources/modules.xml');
    $exist = false;
   
    
    foreach ($modules->module as $module) {
        if (($URI_module === (String) $module->uri)) {
            //print_r($URI_module);
            //exit;
            $exist = true;
            $path = "wp-content/themes/personal/modules/" . $URI_module ."/controller/controller_" . $URI_module . ".class.php";
            
            if (file_exists($path)) {
                $path_utils = "wp-content/themes/personal/modules/" . $URI_module . "/utils/functions_" . $URI_module . ".php";
                include_once($path);

                if (file_exists($path_utils)) {
                    include_once($path_utils);

                }

                $controllerClass = "controller_" . $URI_module;
                $obj = new $controllerClass;
                handlerfunction(((String) $module->name), $obj, $URI_function);
                break;

            }else {
                
                echo "error 404 - not found controller";

            }
            
        }
    }
    if (!$exist) {
        
        echo "error 404 - not found module";

    }
}

function handlerFunction($module, $obj, $URI_function) {
    $functions = simplexml_load_file( "wp-content/themes/personal/modules/" . $module . "/resources/functions.xml");
    $exist = false;
    
    foreach ($functions->function as $function) {
        if (($URI_function === (String) $function->uri)) {
            $exist = true;
            $event = (String) $function->name;
            break;
        }
    }
    if (!$exist) {
        
        echo "error 404 - not found function";

    } else {
        //$obj->$event();
        call_user_func(array($obj, $event));
    }
}

handlerActivate();
handlerRouter();
ob_flush();



